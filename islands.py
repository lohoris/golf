#/usr/bin/env python3

###### Imports

from functions import *
import pandas

###### Input

map = (
	(0, 1, 0, 1, 0, 0, 0),
	(0, 1, 1, 1, 0, 0, 0),
	(0, 0, 0, 0, 1, 0, 0),
	(0, 0, 0, 0, 1, 0, 0),
	(0, 0, 0, 0, 0, 0, 0),
	(0, 1, 1, 0, 0, 0, 0),
)

print(pandas.DataFrame(map))

###### Functions

def merge_islands ( dest, orig, res_map ):
	#print(("merging islands",dest,orig,len(res_map),len(res_map[1])))
	for y in range(0, len(res_map)):
		for x in range(0, len(res_map[y])):
			val = res_map[y][x]
			if val==orig:
				res_map[y][x] = dest

def get_island_number ( point_y, point_x, res_map ):
	current_number = 0
	for delta in ( (-1,0), (1,0), (0,-1), (0,1) ):
		(dy,dx) = delta
		check_y = point_y+dy
		check_x = point_x+dx
		
		try:
			check_val = res_map[check_y][check_x]
		except KeyError:
			# This cell hasn't been checked yet, and that's fine
			continue
		
		if check_val==0:
			# Sea
			continue
		
		# Ok the checked cell is Land
		
		if current_number==check_val:
			# It's already on this Island, nothing to do
			continue
		
		if current_number:
			# We have now to merge two Islands
			
			merge_islands( current_number, check_val, res_map )
		else:
			# Ok we've found the Island we are in
			
			current_number = check_val
		
		#
	
	if current_number==0:
		# It's a new Island
		return 0
	
	return current_number

###### Main

## Assigns the numbers

res = {}
island_num = 1
for y in range(0, len(map)):
	res[y] = {}
	for x in range(0, len(map[y])):
		origin = map[y][x]
		printn(str(origin))
		
		if origin==0:
			dest = 0
		else:
			dest = get_island_number(y,x,res)
			if dest==0:
				# This is a new island
				dest = island_num
				island_num = island_num+1
		
		res[y][x] = dest
	printn("\n")

print(pandas.DataFrame(res))

## Counts the numbers

numbers = {}
for y in range(0, len(res)):
	printn("\n")
	for x in range(0, len(res[y])):
		val = res[y][x]
		printn(val)
		if val!=0:
			numbers[val] = True
printn("\n")

printn("\n")
printn("### There are "+str(len(numbers))+" different islands in this map ###\n")
