#/usr/bin/env python3

###### Imports

#from functions import *

###### Input

nums = [6, 6, 4, 7, 5, 7, 8, 8, 6, 6, 5]
print('Input: '+str(nums))

###### Functions

###### Main

## Checks which numbers appear an odd amount of times

odds = set()
index = {}
for num in nums:
	if num in index:
		index[num] = index[num]+1
	else:
		index[num] = 1
	if index[num]%2:
		print('y',num)
		odds.add(num)
	else:
		print('n',num)
		odds.remove(num)
print('Index: '+str(index))
print('Odds: '+str(odds))

## Gets the only odd number (as per requirement)

if len(odds)!=1:
	print('The test lied')
else:
	print('Result: '+str(odds.pop()))
