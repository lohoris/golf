#/usr/bin/env python3

###### Imports

from functions import *

###### Input

nums = [-1, 8, 6, 1, 2, 3, 4]
target = 7
printn('Input: '+str(nums)+'\n')

###### Functions

###### Main

index = {}
for num in nums:
	if num in index:
		index[num] = index[num]+1
	else:
		index[num] = 1
printn('Index: '+str(index)+'\n')

res = []
for num in nums:
	corr = target-num
	if corr>num:
		# In order to avoid duplicates
		continue
	
	if corr==num:
		# Special case, let's check if it occurs twice or not
		if index[num]<2:
			continue
	
	if corr in index:
		tup = (num,corr)
		if tup in res:
			# Otherwise tuples like (4,4) with target 8 will appear twice in res
			continue
		res.append(tup)
printn('Result: '+str(res)+'\n')
